package com.example.newsjsoup.list

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager

import com.example.newsjsoup.R
import kotlinx.android.synthetic.main.list_news_fragment.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jsoup.Jsoup
import java.io.IOException

class ListNewsFragment : Fragment() {

    private val url = "https://www.volzsky.ru/index.php?wx=16"
    private val listNews = mutableListOf<News>()
    private lateinit var adapter: DataAdapter

    companion object {
        fun newInstance() = ListNewsFragment()
    }

    private lateinit var viewModel: ListNewsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.list_news_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ListNewsViewModel::class.java)
        // TODO: Use the ViewModel

        adapter = DataAdapter()
        val llm = LinearLayoutManager(this.context)

        rv.layoutManager = llm
        rv.adapter = adapter

        GlobalScope.launch {
            getData()
        }
    }

    private fun getData() {
        try {

            val document = Jsoup.connect(url).get()
            val element = document.select("div[class=btc_block-1]")

            for (i in 0 until element.size){
                val title = element.select("div[class=btc_block-1_1 btc_h]")
                    .select("a")
                    .eq(i)
                    .text()

                val description = element.select("div[class=btc_block-1_2]")
                    .select("btc_p")
                    .eq(i)
                    .text()

                val image =
                    document.baseUri() +
                        element.select("div[class=btc_block-1_2]")
                            .select("img")
                            .eq(i)
                            .attr("src")

                val additionalInfo = element.select("div[class=btc_block-1_1 btc_h]")
                    .select("btc_p btc_inf")
                    .eq(i)
                    .text()

                val linkDetails =
                    document.baseUri() +
                        element.select("div[class=btc_block-1_2]")
                        .select("a")
                        .eq(i)
                        .attr("href")

                listNews.add(News(title, description,image,additionalInfo, linkDetails))
            }
            GlobalScope.launch(Dispatchers.Main){
                adapter.set(listNews)
            }

        }catch (e: IOException){}

    }

}
